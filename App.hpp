#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include <string>
#include <iostream>
#include "Raupe.hpp"
#include "Level.hpp"

class App {
	private:
		bool Running;
		SDL_Window *window;
		SDL_Renderer *renderer;
		SDL_Texture *tBackground;

		SDL_Texture *loadTexture(std::string path);
		bool loadBackground();
		void DetermineLevelPosition();

		int WindowWidth;
		int WindowHight;
		
		double ScaleW;
		double ScaleH;

		int LevelOffsetx;
		int LevelOffsety;
		double LevelScale;
		int LevelH;
		int LevelW;

		Raupe *raup;
		Level *level;

	public:
		App();
		App(int x, int y);
		bool OnExecute();
		bool OnInit();
		void OnEvent(SDL_Event* Event);
		void OnLoop();
		void OnRender();
		void OnCleanup();
};
