#include "Level.hpp"
Level::Level(SDL_Renderer *renderer,int *Offsetx, int *Offsety,
		double *Scale,
		std::string pathTexture, std::string pathSeenTexture){
	Lhight=24;
	Lwidth=64;
	LTwidth=1240;
	LThight=480;
	LOffsetx=Offsetx;
	LOffsety=Offsety;
	LScale=Scale;
	colliderMatrix=new std::vector<bool>(Lhight*Lwidth);
	LTextureSeen=loadTexture(renderer,pathSeenTexture);
	determinecolliderMatrix(pathTexture);
	determineStartplace();
}
Level::~Level(){
	free();
}
SDL_Texture *Level::loadTexture(SDL_Renderer *renderer,std::string path){
	SDL_Texture *newTexture = NULL;

	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if(!loadedSurface){
		return NULL;
	}
	newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
	if(!newTexture){
		return NULL;
	}
	SDL_FreeSurface(loadedSurface);
	return newTexture;
}
void Level::render(SDL_Renderer *renderer){
	double Scale=(*LScale);
	SDL_Rect renderQuadTar = {
		(*LOffsetx),
		(*LOffsety), 
		(int) ((Scale)*(double)LTwidth), 
		(int) ((Scale)*(double)LThight)};

	SDL_RenderCopy(renderer,LTextureSeen,NULL,&renderQuadTar);
}
int Level::getStartplacex(){
	return Lxb;
}
int Level::getStartplacey(){
	return Lyb;
}
int Level::getTextureWidth(){
	return Lwidth;
}
int Level::getTextureHight(){
	return Lhight;
}
int Level::getSeenTextureWidth(){
	return LTwidth;
}
int Level::getSeenTextureHight(){
	return LThight;
}
std::vector<bool> *Level::getcolliderMatrix(){
	return colliderMatrix;
}
void Level::free(){
	if(LTextureSeen!=NULL){
		SDL_DestroyTexture(LTextureSeen);
	}
	LTextureSeen=NULL;
}
void Level::determinecolliderMatrix(std::string path){
	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
	Uint8 *pixels =NULL;
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
	}
	else
	{
		if(loadedSurface->format->BytesPerPixel!=1){
			printf("The image %s has the wrong format. It must be grayscale", path.c_str());
			std::exit(1);
		}
		pixels = (Uint8 *) loadedSurface->pixels;
		//Get rid of old loaded surface
		Lwidth=loadedSurface->w;
		Lhight=loadedSurface->h;

	
		//How many pixels are saved in a row which can
		//be more than Lwidth
		int pitch=loadedSurface->pitch;
		for (int i=0;i<Lhight;i++){
			for (int j=0;j<Lwidth;j++){
				(*colliderMatrix)[j+i*Lwidth]=false;
				if(pixels[j+i*pitch]!=0){
					(*colliderMatrix)[j+i*Lwidth]=true;
				}
			}
		}		
	SDL_FreeSurface( loadedSurface );
	}
}
void Level::determineStartplace(){
	Lxb=0;
	Lyb=0;
	for (int i=Lhight-1;i>=0;i--){
		for (int j=0;j<Lwidth;j++){
		//	std::cout<<i <<" "<< j << std::endl;
			if((*colliderMatrix)[j+i*Lwidth]==false){
				Lxb=j;
				Lyb=i;
			//	std::cout<<i <<" "<< j << std::endl;
				return;
			}
		}
	}
}
