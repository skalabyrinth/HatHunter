#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include <string>
#include <cmath>
#include <vector>
#include <iostream>
#include <cstdlib>

class Level{
	/* not hight and width of Texture image but 
	hight and width of the collision rectangle image*/
	int Lhight;
	int Lwidth;

	int Lxb,Lyb;/*Back
	coordinates of the Raupe to start in the level*/
	std::vector<bool> *colliderMatrix;

	/*Level Texture, that is been seen*/
	SDL_Texture* LTextureSeen;
	int *LOffsetx;
	int *LOffsety;
	double *LScale;
	int LThight;
	int LTwidth;

	SDL_Texture *loadTexture(SDL_Renderer *renderer,
			std::string path);
	void determinecolliderMatrix(std::string path);
	void determineStartplace();

	public:
	Level(SDL_Renderer *renderer,int *Offsetx, int *Offsety,
			double *Scale,
			std::string pathTexture, 
			std::string pathSeenTexture);
	~Level();
	void render(SDL_Renderer *renderer);
	int getStartplacex();
	int getStartplacey();
	int getTextureWidth();
	int getTextureHight();
	int getSeenTextureWidth();
	int getSeenTextureHight();
	std::vector<bool> *getcolliderMatrix();
	void free();

};
