#include "App.hpp"

App::App() {
	window = NULL;
	renderer = NULL;
	tBackground = NULL;
	Running = true;
	WindowWidth=1240;
	WindowHight=480;
	ScaleW=1.0;
	ScaleH=1.0;
	LevelOffsetx=0;
	LevelOffsety=0;
	LevelH=0;
	LevelW=0;
}
App::App(int x, int y){
	window = NULL;
	renderer = NULL;
	tBackground = NULL;
	Running = true;
	WindowWidth=x;
	WindowHight=y;
	ScaleW=1.0;
	ScaleH=1.0;
	LevelOffsetx=0;
	LevelOffsety=0;
	LevelH=0;
	LevelW=0;

}

bool App::OnExecute() {
	if (!OnInit()){
		return false;
	}

	if (!loadBackground()){
		return false;
	}	

	SDL_Event Event;

	Uint32 time=SDL_GetTicks();
	while(Running){
		while(SDL_PollEvent(&Event)){
			OnEvent(&Event);
		}
		Uint32 steps=SDL_GetTicks()-time;
		time+=steps;
		for(Uint32 i=0; i<steps; i++){
			OnLoop();
		}
		OnRender();
	}
	OnCleanup();
	return true;
}

bool App::OnInit(){
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0){
		return false;
	}

	window = SDL_CreateWindow("HatHunter",
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			WindowWidth,WindowHight,
			0);
	if(!window){
		return false;
	}
	renderer = SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);
	if(!renderer){
		return false;
	}
	SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);

	if( !(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG)){
		return false;
	}
	level= new Level(renderer,&LevelOffsetx,&LevelOffsety,&LevelScale,
			"img/Level1.png","img/TextureLevel1.png");
	LevelW=level->getSeenTextureWidth();
	LevelH=level->getSeenTextureHight();
	raup = new Raupe(renderer,&LevelScale,&LevelOffsetx,&LevelOffsety,
			&LevelW,&LevelH);
	raup->setcolliderMatrix(level->getcolliderMatrix(),
			level->getTextureWidth(),
			level->getTextureHight());

	DetermineLevelPosition();
	/*Set Startposition to the lowest rightest place without collision. Hope
	 * the level has there space enogh for the Raupe raup*/
	int x=level->getStartplacex();
	int y=level->getStartplacey();
	int LevelHight=level->getTextureHight();
	int LevelWidth=level->getTextureWidth();
	x=(int)((double)(x*WindowWidth)
			/((double)LevelWidth));
	y=(int)((double)((y+1)*WindowHight)
			/((double)LevelHight));
	y=y-raup->getThight();

	//	std::cout << x << " " << y << std::endl;
	raup->setx(x);
	raup->sety(y);

	return true;
}

void App::OnEvent(SDL_Event* Event){
	if (Event->type == SDL_QUIT){
		Running = false;
	}else
	{
		switch(Event->type){
			case SDL_KEYDOWN :
				switch(Event->key.keysym.sym)
				{
					case SDLK_SPACE:
						break;
						
					case SDLK_LSHIFT:
						raup->userswitched=true;
						break;
					case SDLK_RSHIFT:
						raup->userswitched=true;
						break;

				}//end switch key.keysym.sym
				break;

			case SDL_WINDOWEVENT :
				Uint32 windowID=SDL_GetWindowID(window);
				if(Event->window.windowID == windowID){
				switch(Event->window.event)
				{
					case SDL_WINDOWEVENT_RESIZED :
						int W=SDL_GetWindowSurface(window)->w;
						int H=SDL_GetWindowSurface(window)->h;
						ScaleW=((double) W)/((double) WindowWidth);
						ScaleH=((double) H)/((double) WindowHight);

						DetermineLevelPosition();
						
						break;					
				}// end switch window.event
				}
				break;
		}//end Event->type
	}//end else if not quit
}

void App::OnLoop() {
	raup->move();
	const Uint8* currentKeyStates = SDL_GetKeyboardState( NULL );
	if( currentKeyStates[ SDL_SCANCODE_LEFT ] )
	{
		raup->movingLtR=false;
		raup->movingRtL=true;
	}else
	{
		raup->movingRtL=false;
	}
	if( currentKeyStates[ SDL_SCANCODE_RIGHT ] )
	{
		raup->movingLtR=true;
		raup->movingRtL=false;
	}else
	{
		raup->movingLtR=false;
	}

	if( currentKeyStates[ SDL_SCANCODE_UP ] ){
		raup->movingUp=true;
		raup->movingDown=false;
	}else{
		raup->movingUp=false;
	}
	if( currentKeyStates[ SDL_SCANCODE_DOWN ] ){
		raup->movingDown=true;
		raup->movingUp=false;
	}else{
		raup->movingDown=false;
	}
}

void App::OnRender() {
	SDL_RenderClear(renderer);
	SDL_RenderCopy(renderer,tBackground,NULL,NULL);
	level->render(renderer);
	raup->render(renderer);

	SDL_RenderPresent(renderer);
}

void App::OnCleanup() {
	SDL_DestroyTexture(tBackground);
	tBackground=NULL;
	raup->free();
	delete raup;
	level->free();
	delete level;

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	renderer=NULL;
	window=NULL;

	IMG_Quit();
	SDL_Quit();
}


SDL_Texture *App::loadTexture(std::string path){
	SDL_Texture *newTexture = NULL;

	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if(!loadedSurface){
		return NULL;
	}
	newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
	if(!newTexture){
		return NULL;
	}
	SDL_FreeSurface(loadedSurface);
	return newTexture;
}

bool App::loadBackground()
{
	tBackground = loadTexture("img/Background.png");
	if(!tBackground)
	{
		return false;
	}
	return true;
}
void App::DetermineLevelPosition(){
	LevelScale=(ScaleW<ScaleH)? ScaleW : ScaleH;
	double Temp1=ScaleW*((double) WindowWidth);
	double Temp2=LevelScale*((double) level->getSeenTextureWidth());
	LevelOffsetx=(int) (Temp1-Temp2)/2;
	
	Temp1=ScaleH*((double) WindowHight);
	Temp2=LevelScale*((double) level->getSeenTextureHight());
	LevelOffsety=(int) (Temp1-Temp2)/2;
}
